// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.4.2 (swiftlang-1205.0.28.2 clang-1205.0.19.57)
// swift-module-flags: -target armv7-apple-ios10.0 -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name Karza_BigInt
import Foundation
import Swift
extension BigUInt {
  public static func + (a: Karza_BigInt.BigUInt, b: Karza_BigInt.BigUInt) -> Karza_BigInt.BigUInt
  public static func += (a: inout Karza_BigInt.BigUInt, b: Karza_BigInt.BigUInt)
}
extension BigInt {
  public static func + (a: Karza_BigInt.BigInt, b: Karza_BigInt.BigInt) -> Karza_BigInt.BigInt
  public static func += (a: inout Karza_BigInt.BigInt, b: Karza_BigInt.BigInt)
}
public struct BigUInt : Swift.UnsignedInteger {
  public typealias Word = Swift.UInt
  public init()
  public init(words: [Karza_BigInt.BigUInt.Word])
  public typealias Magnitude = Karza_BigInt.BigUInt
}
extension BigUInt {
  public static var isSigned: Swift.Bool {
    get
  }
  public func signum() -> Karza_BigInt.BigUInt
}
extension BigUInt {
  public static func randomInteger<RNG>(withMaximumWidth width: Swift.Int, using generator: inout RNG) -> Karza_BigInt.BigUInt where RNG : Swift.RandomNumberGenerator
  public static func randomInteger(withMaximumWidth width: Swift.Int) -> Karza_BigInt.BigUInt
  public static func randomInteger<RNG>(withExactWidth width: Swift.Int, using generator: inout RNG) -> Karza_BigInt.BigUInt where RNG : Swift.RandomNumberGenerator
  public static func randomInteger(withExactWidth width: Swift.Int) -> Karza_BigInt.BigUInt
  public static func randomInteger<RNG>(lessThan limit: Karza_BigInt.BigUInt, using generator: inout RNG) -> Karza_BigInt.BigUInt where RNG : Swift.RandomNumberGenerator
  public static func randomInteger(lessThan limit: Karza_BigInt.BigUInt) -> Karza_BigInt.BigUInt
}
extension BigUInt {
  public subscript(bitAt index: Swift.Int) -> Swift.Bool {
    get
    set
  }
}
extension BigUInt {
  public var bitWidth: Swift.Int {
    get
  }
  public var leadingZeroBitCount: Swift.Int {
    get
  }
  public var trailingZeroBitCount: Swift.Int {
    get
  }
}
extension BigInt {
  public var bitWidth: Swift.Int {
    get
  }
  public var trailingZeroBitCount: Swift.Int {
    get
  }
}
extension BigUInt {
  public struct Words : Swift.RandomAccessCollection {
    public var startIndex: Swift.Int {
      get
    }
    public var endIndex: Swift.Int {
      get
    }
    public subscript(index: Swift.Int) -> Karza_BigInt.BigUInt.Word {
      get
    }
    public typealias Element = Karza_BigInt.BigUInt.Word
    public typealias Index = Swift.Int
    public typealias Indices = Swift.Range<Swift.Int>
    public typealias Iterator = Swift.IndexingIterator<Karza_BigInt.BigUInt.Words>
    public typealias SubSequence = Swift.Slice<Karza_BigInt.BigUInt.Words>
  }
  public var words: Karza_BigInt.BigUInt.Words {
    get
  }
  public init<Words>(words: Words) where Words : Swift.Sequence, Words.Element == Karza_BigInt.BigUInt.Word
}
extension BigInt {
  public struct Words : Swift.RandomAccessCollection {
    public typealias Indices = Swift.CountableRange<Swift.Int>
    public var count: Swift.Int {
      get
    }
    public var indices: Karza_BigInt.BigInt.Words.Indices {
      get
    }
    public var startIndex: Swift.Int {
      get
    }
    public var endIndex: Swift.Int {
      get
    }
    public subscript(index: Swift.Int) -> Swift.UInt {
      get
    }
    public typealias Element = Swift.UInt
    public typealias Index = Swift.Int
    public typealias Iterator = Swift.IndexingIterator<Karza_BigInt.BigInt.Words>
    public typealias SubSequence = Swift.Slice<Karza_BigInt.BigInt.Words>
  }
  public var words: Karza_BigInt.BigInt.Words {
    get
  }
  public init<S>(words: S) where S : Swift.Sequence, S.Element == Karza_BigInt.BigInt.Word
}
public struct BigInt : Swift.SignedInteger {
  public enum Sign {
    case plus
    case minus
    public static func == (a: Karza_BigInt.BigInt.Sign, b: Karza_BigInt.BigInt.Sign) -> Swift.Bool
    public func hash(into hasher: inout Swift.Hasher)
    public var hashValue: Swift.Int {
      get
    }
  }
  public typealias Magnitude = Karza_BigInt.BigUInt
  public typealias Word = Karza_BigInt.BigUInt.Word
  public static var isSigned: Swift.Bool {
    get
  }
  public var magnitude: Karza_BigInt.BigUInt
  public var sign: Karza_BigInt.BigInt.Sign
  public init(sign: Karza_BigInt.BigInt.Sign, magnitude: Karza_BigInt.BigUInt)
  public var isZero: Swift.Bool {
    get
  }
  public func signum() -> Karza_BigInt.BigInt
}
extension BigUInt {
  public func isStrongProbablePrime(_ base: Karza_BigInt.BigUInt) -> Swift.Bool
  public func isPrime(rounds: Swift.Int = 10) -> Swift.Bool
}
extension BigInt {
  public func isStrongProbablePrime(_ base: Karza_BigInt.BigInt) -> Swift.Bool
  public func isPrime(rounds: Swift.Int = 10) -> Swift.Bool
}
extension BigUInt {
  public init?<T>(exactly source: T) where T : Swift.BinaryFloatingPoint
  public init<T>(_ source: T) where T : Swift.BinaryFloatingPoint
}
extension BigInt {
  public init?<T>(exactly source: T) where T : Swift.BinaryFloatingPoint
  public init<T>(_ source: T) where T : Swift.BinaryFloatingPoint
}
extension BinaryFloatingPoint where Self.RawExponent : Swift.FixedWidthInteger, Self.RawSignificand : Swift.FixedWidthInteger {
  public init(_ value: Karza_BigInt.BigInt)
  public init(_ value: Karza_BigInt.BigUInt)
}
extension BigUInt : Swift.Hashable {
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension BigInt : Swift.Hashable {
  public func hash(into hasher: inout Swift.Hasher)
  public var hashValue: Swift.Int {
    get
  }
}
extension BigUInt {
  public func greatestCommonDivisor(with b: Karza_BigInt.BigUInt) -> Karza_BigInt.BigUInt
  public func inverse(_ modulus: Karza_BigInt.BigUInt) -> Karza_BigInt.BigUInt?
}
extension BigInt {
  public func greatestCommonDivisor(with b: Karza_BigInt.BigInt) -> Karza_BigInt.BigInt
  public func inverse(_ modulus: Karza_BigInt.BigInt) -> Karza_BigInt.BigInt?
}
extension BigUInt {
  public init?<T>(exactly source: T) where T : Swift.BinaryInteger
  public init<T>(_ source: T) where T : Swift.BinaryInteger
  public init<T>(truncatingIfNeeded source: T) where T : Swift.BinaryInteger
  public init<T>(clamping source: T) where T : Swift.BinaryInteger
}
extension BigInt {
  public init()
  public init(_ integer: Karza_BigInt.BigUInt)
  public init<T>(_ source: T) where T : Swift.BinaryInteger
  public init?<T>(exactly source: T) where T : Swift.BinaryInteger
  public init<T>(clamping source: T) where T : Swift.BinaryInteger
  public init<T>(truncatingIfNeeded source: T) where T : Swift.BinaryInteger
}
extension BigUInt : Swift.ExpressibleByIntegerLiteral {
  public init(integerLiteral value: Swift.UInt64)
  public typealias IntegerLiteralType = Swift.UInt64
}
extension BigInt : Swift.ExpressibleByIntegerLiteral {
  public init(integerLiteral value: Swift.Int64)
  public typealias IntegerLiteralType = Swift.Int64
}
extension BigUInt {
  public init?<S>(_ text: S, radix: Swift.Int = 10) where S : Swift.StringProtocol
}
extension BigInt {
  public init?<S>(_ text: S, radix: Swift.Int = 10) where S : Swift.StringProtocol
}
extension String {
  public init(_ v: Karza_BigInt.BigUInt)
  public init(_ v: Karza_BigInt.BigUInt, radix: Swift.Int, uppercase: Swift.Bool = false)
  public init(_ value: Karza_BigInt.BigInt, radix: Swift.Int = 10, uppercase: Swift.Bool = false)
}
extension BigUInt : Swift.ExpressibleByStringLiteral {
  public init(unicodeScalarLiteral value: Swift.UnicodeScalar)
  public init(extendedGraphemeClusterLiteral value: Swift.String)
  public init(stringLiteral value: Swift.StringLiteralType)
  public typealias ExtendedGraphemeClusterLiteralType = Swift.String
  public typealias StringLiteralType = Swift.StringLiteralType
  public typealias UnicodeScalarLiteralType = Swift.UnicodeScalar
}
extension BigInt : Swift.ExpressibleByStringLiteral {
  public init(unicodeScalarLiteral value: Swift.UnicodeScalar)
  public init(extendedGraphemeClusterLiteral value: Swift.String)
  public init(stringLiteral value: Swift.StringLiteralType)
  public typealias ExtendedGraphemeClusterLiteralType = Swift.String
  public typealias StringLiteralType = Swift.StringLiteralType
  public typealias UnicodeScalarLiteralType = Swift.UnicodeScalar
}
extension BigUInt : Swift.CustomStringConvertible {
  public var description: Swift.String {
    get
  }
}
extension BigInt : Swift.CustomStringConvertible {
  public var description: Swift.String {
    get
  }
}
extension BigUInt : Swift.CustomPlaygroundDisplayConvertible {
  public var playgroundDescription: Any {
    get
  }
}
extension BigInt : Swift.CustomPlaygroundDisplayConvertible {
  public var playgroundDescription: Any {
    get
  }
}
extension BigUInt {
  public func quotientAndRemainder(dividingBy y: Karza_BigInt.BigUInt) -> (quotient: Karza_BigInt.BigUInt, remainder: Karza_BigInt.BigUInt)
  public static func / (x: Karza_BigInt.BigUInt, y: Karza_BigInt.BigUInt) -> Karza_BigInt.BigUInt
  public static func % (x: Karza_BigInt.BigUInt, y: Karza_BigInt.BigUInt) -> Karza_BigInt.BigUInt
  public static func /= (x: inout Karza_BigInt.BigUInt, y: Karza_BigInt.BigUInt)
  public static func %= (x: inout Karza_BigInt.BigUInt, y: Karza_BigInt.BigUInt)
}
extension BigInt {
  public func quotientAndRemainder(dividingBy y: Karza_BigInt.BigInt) -> (quotient: Karza_BigInt.BigInt, remainder: Karza_BigInt.BigInt)
  public static func / (a: Karza_BigInt.BigInt, b: Karza_BigInt.BigInt) -> Karza_BigInt.BigInt
  public static func % (a: Karza_BigInt.BigInt, b: Karza_BigInt.BigInt) -> Karza_BigInt.BigInt
  public func modulus(_ mod: Karza_BigInt.BigInt) -> Karza_BigInt.BigInt
}
extension BigInt {
  public static func /= (a: inout Karza_BigInt.BigInt, b: Karza_BigInt.BigInt)
  public static func %= (a: inout Karza_BigInt.BigInt, b: Karza_BigInt.BigInt)
}
extension BigUInt {
  public static func >>= <Other>(lhs: inout Karza_BigInt.BigUInt, rhs: Other) where Other : Swift.BinaryInteger
  public static func <<= <Other>(lhs: inout Karza_BigInt.BigUInt, rhs: Other) where Other : Swift.BinaryInteger
  public static func >> <Other>(lhs: Karza_BigInt.BigUInt, rhs: Other) -> Karza_BigInt.BigUInt where Other : Swift.BinaryInteger
  public static func << <Other>(lhs: Karza_BigInt.BigUInt, rhs: Other) -> Karza_BigInt.BigUInt where Other : Swift.BinaryInteger
}
extension BigInt {
  public static func &<< (left: Karza_BigInt.BigInt, right: Karza_BigInt.BigInt) -> Karza_BigInt.BigInt
  public static func &<<= (left: inout Karza_BigInt.BigInt, right: Karza_BigInt.BigInt)
  public static func &>> (left: Karza_BigInt.BigInt, right: Karza_BigInt.BigInt) -> Karza_BigInt.BigInt
  public static func &>>= (left: inout Karza_BigInt.BigInt, right: Karza_BigInt.BigInt)
  public static func << <Other>(lhs: Karza_BigInt.BigInt, rhs: Other) -> Karza_BigInt.BigInt where Other : Swift.BinaryInteger
  public static func <<= <Other>(lhs: inout Karza_BigInt.BigInt, rhs: Other) where Other : Swift.BinaryInteger
  public static func >> <Other>(lhs: Karza_BigInt.BigInt, rhs: Other) -> Karza_BigInt.BigInt where Other : Swift.BinaryInteger
  public static func >>= <Other>(lhs: inout Karza_BigInt.BigInt, rhs: Other) where Other : Swift.BinaryInteger
}
extension BigUInt {
  public mutating func subtractReportingOverflow(_ b: Karza_BigInt.BigUInt, shiftedBy shift: Swift.Int = 0) -> Swift.Bool
  public func subtractingReportingOverflow(_ other: Karza_BigInt.BigUInt, shiftedBy shift: Swift.Int) -> (partialValue: Karza_BigInt.BigUInt, overflow: Swift.Bool)
  public func subtractingReportingOverflow(_ other: Karza_BigInt.BigUInt) -> (partialValue: Karza_BigInt.BigUInt, overflow: Swift.Bool)
  public mutating func subtract(_ other: Karza_BigInt.BigUInt, shiftedBy shift: Swift.Int = 0)
  public func subtracting(_ other: Karza_BigInt.BigUInt, shiftedBy shift: Swift.Int = 0) -> Karza_BigInt.BigUInt
  public mutating func decrement(shiftedBy shift: Swift.Int = 0)
  public static func - (a: Karza_BigInt.BigUInt, b: Karza_BigInt.BigUInt) -> Karza_BigInt.BigUInt
  public static func -= (a: inout Karza_BigInt.BigUInt, b: Karza_BigInt.BigUInt)
}
extension BigInt {
  public mutating func negate()
  public static func - (a: Karza_BigInt.BigInt, b: Karza_BigInt.BigInt) -> Karza_BigInt.BigInt
  public static func -= (a: inout Karza_BigInt.BigInt, b: Karza_BigInt.BigInt)
}
extension BigUInt {
  public mutating func multiply(byWord y: Karza_BigInt.BigUInt.Word)
  public func multiplied(byWord y: Karza_BigInt.BigUInt.Word) -> Karza_BigInt.BigUInt
  public mutating func multiplyAndAdd(_ x: Karza_BigInt.BigUInt, _ y: Karza_BigInt.BigUInt.Word, shiftedBy shift: Swift.Int = 0)
  public func multiplied(by y: Karza_BigInt.BigUInt) -> Karza_BigInt.BigUInt
  public static var directMultiplicationLimit: Swift.Int
  public static func * (x: Karza_BigInt.BigUInt, y: Karza_BigInt.BigUInt) -> Karza_BigInt.BigUInt
  public static func *= (a: inout Karza_BigInt.BigUInt, b: Karza_BigInt.BigUInt)
}
extension BigInt {
  public static func * (a: Karza_BigInt.BigInt, b: Karza_BigInt.BigInt) -> Karza_BigInt.BigInt
  public static func *= (a: inout Karza_BigInt.BigInt, b: Karza_BigInt.BigInt)
}
extension BigUInt : Swift.Strideable {
  public typealias Stride = Karza_BigInt.BigInt
  public func advanced(by n: Karza_BigInt.BigInt) -> Karza_BigInt.BigUInt
  public func distance(to other: Karza_BigInt.BigUInt) -> Karza_BigInt.BigInt
}
extension BigInt : Swift.Strideable {
  public typealias Stride = Karza_BigInt.BigInt
  public func advanced(by n: Karza_BigInt.BigInt.Stride) -> Karza_BigInt.BigInt
  public func distance(to other: Karza_BigInt.BigInt) -> Karza_BigInt.BigInt.Stride
}
extension BigUInt {
  public func squareRoot() -> Karza_BigInt.BigUInt
}
extension BigInt {
  public func squareRoot() -> Karza_BigInt.BigInt
}
extension BigUInt {
  public init(_ buffer: Swift.UnsafeRawBufferPointer)
  public init(_ data: Foundation.Data)
  public func serialize() -> Foundation.Data
}
extension BigInt {
  public init(_ buffer: Swift.UnsafeRawBufferPointer)
  public init(_ data: Foundation.Data)
  public func serialize() -> Foundation.Data
}
extension BigUInt : Swift.Comparable {
  public static func compare(_ a: Karza_BigInt.BigUInt, _ b: Karza_BigInt.BigUInt) -> Foundation.ComparisonResult
  public static func == (a: Karza_BigInt.BigUInt, b: Karza_BigInt.BigUInt) -> Swift.Bool
  public static func < (a: Karza_BigInt.BigUInt, b: Karza_BigInt.BigUInt) -> Swift.Bool
}
extension BigInt {
  public static func == (a: Karza_BigInt.BigInt, b: Karza_BigInt.BigInt) -> Swift.Bool
  public static func < (a: Karza_BigInt.BigInt, b: Karza_BigInt.BigInt) -> Swift.Bool
}
extension BigInt : Swift.Codable {
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
extension BigUInt : Swift.Codable {
  public init(from decoder: Swift.Decoder) throws
  public func encode(to encoder: Swift.Encoder) throws
}
extension BigUInt {
  public func power(_ exponent: Swift.Int) -> Karza_BigInt.BigUInt
  public func power(_ exponent: Karza_BigInt.BigUInt, modulus: Karza_BigInt.BigUInt) -> Karza_BigInt.BigUInt
}
extension BigInt {
  public func power(_ exponent: Swift.Int) -> Karza_BigInt.BigInt
  public func power(_ exponent: Karza_BigInt.BigInt, modulus: Karza_BigInt.BigInt) -> Karza_BigInt.BigInt
}
extension BigUInt {
  prefix public static func ~ (a: Karza_BigInt.BigUInt) -> Karza_BigInt.BigUInt
  public static func |= (a: inout Karza_BigInt.BigUInt, b: Karza_BigInt.BigUInt)
  public static func &= (a: inout Karza_BigInt.BigUInt, b: Karza_BigInt.BigUInt)
  public static func ^= (a: inout Karza_BigInt.BigUInt, b: Karza_BigInt.BigUInt)
}
extension BigInt {
  prefix public static func ~ (x: Karza_BigInt.BigInt) -> Karza_BigInt.BigInt
  public static func & (lhs: inout Karza_BigInt.BigInt, rhs: Karza_BigInt.BigInt) -> Karza_BigInt.BigInt
  public static func | (lhs: inout Karza_BigInt.BigInt, rhs: Karza_BigInt.BigInt) -> Karza_BigInt.BigInt
  public static func ^ (lhs: inout Karza_BigInt.BigInt, rhs: Karza_BigInt.BigInt) -> Karza_BigInt.BigInt
  public static func &= (lhs: inout Karza_BigInt.BigInt, rhs: Karza_BigInt.BigInt)
  public static func |= (lhs: inout Karza_BigInt.BigInt, rhs: Karza_BigInt.BigInt)
  public static func ^= (lhs: inout Karza_BigInt.BigInt, rhs: Karza_BigInt.BigInt)
}
extension Karza_BigInt.BigInt.Sign : Swift.Equatable {}
extension Karza_BigInt.BigInt.Sign : Swift.Hashable {}
